// Rovinsmon provides a TUI to monitor the output from the Rovins INS
package main

import (
	"errors"
	"flag"
	"fmt"
	"math"
	"net"
	"net/url"
	"os"
	"runtime"
	"time"

	"bitbucket.org/uwaploe/ixblue"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
	stan "github.com/nats-io/stan.go"
)

const Usage = `Usage: rovinsmon [options] url

Realtime display of Rovins output data. Valid urls are:

tcp://host:port  -  read data directly from the Rovins INS
nats//host:port/subject  -  read data from the NATS message broker

`

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	clusterID string = "must-cluster"
)

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.StringVar(&clusterID, "cid", lookupEnvOrString("NATS_CLUSTER_ID", clusterID),
		"NATS cluster ID")

	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

type model struct {
	u      *url.URL
	src    chan ixblue.DataRecord
	done   chan struct{}
	err    error
	tstamp time.Time
	att    ixblue.AttitudeHdg
	rot    ixblue.VehRotRate
	acc    ixblue.VehAccel
	pos    ixblue.Position
	speed  ixblue.GeoSpeed
	temp   ixblue.Temperature
	status ixblue.AlgStatus
	dvl    ixblue.DvlGndSpeed
}

func listenForData(dec *ixblue.Decoder, ch chan ixblue.DataRecord, done chan struct{}) tea.Cmd {
	return func() tea.Msg {
		for {
			select {
			case <-done:
				return struct{}{}
			default:
			}
			rec, err := dec.Decode()
			if err != nil {
				continue
			}
			ch <- rec
		}
	}
}

type errMsg error

func subscribeToData(conn stan.Conn, subject string, ch chan ixblue.DataRecord,
	done chan struct{}) tea.Cmd {
	return func() tea.Msg {
		insCb := func(m *stan.Msg) {
			rec := ixblue.DataRecord{}
			rec.UnmarshalBinary(m.Data)
			ch <- rec
		}
		if sub, err := conn.Subscribe(subject, insCb); err != nil {
			return errMsg(err)
		} else {
			defer sub.Unsubscribe()
		}

		<-done
		return struct{}{}
	}
}

func waitForData(ch chan ixblue.DataRecord) tea.Cmd {
	return func() tea.Msg {
		rec := <-ch
		return rec
	}
}

func (m *model) updateData(rec ixblue.DataRecord) {
	m.tstamp = rec.Timestamp()
	rec.ExtractBlocks(&m.att)
	rec.ExtractBlocks(&m.rot)
	rec.ExtractBlocks(&m.acc)
	rec.ExtractBlocks(&m.pos)
	rec.ExtractBlocks(&m.speed)
	rec.ExtractBlocks(&m.temp)
	rec.ExtractBlocks(&m.status)
	rec.ExtractBlocks(&m.dvl)
}

var altRef = map[uint8]string{
	0: "MSL",
	1: "Ellipsoid",
}

func validGps(status ixblue.AlgStatus) bool {
	if (status.Word[1]&ixblue.AlgManualGpsValid) != 0 ||
		(status.Word[0]&ixblue.AlgGpsValid) != 0 {
		return true
	}

	return false
}

func (m model) View() string {
	if m.err != nil {
		return fmt.Sprintf("\nAn error occurred: %v\n\n", m.err)
	}
	style := lipgloss.NewStyle().Bold(true).Width(20).Align(lipgloss.Right)
	alert := lipgloss.NewStyle().Foreground(lipgloss.Color("#ff0000"))

	const TimeFormat = "2006-01-02 15:04:05.000Z07:00"
	s := fmt.Sprintf("ixBlue Rovins Status: %s\n\n", m.tstamp.Format(TimeFormat))
	lon := m.pos.Lon
	if lon > 180 {
		lon -= 360
	}

	if !validGps(m.status) {
		s += fmt.Sprintf("%s: %s=%.6f %s=%.6f Alt=%.2f (%s)\n",
			style.Render("GNSS"),
			alert.Render("Lat"), m.pos.Lat,
			alert.Render("Lon"), lon,
			m.pos.Alt, altRef[m.pos.AltRef])
	} else {
		s += fmt.Sprintf("%s: Lat=%.6f Lon=%.6f Alt=%.2f (%s)\n",
			style.Render("GNSS"),
			m.pos.Lat, lon, m.pos.Alt, altRef[m.pos.AltRef])
	}
	s += fmt.Sprintf("%s: East=%.2f North=%.2f Up=%.2f\n",
		style.Render("Speed (m/s)"),
		m.speed.East, m.speed.North, m.speed.Up)
	course := math.Atan2(float64(m.speed.East), float64(m.speed.North)) * 180. / math.Pi
	s += fmt.Sprintf("%s: %.1f\n",
		style.Render("Course (°)"),
		course)
	s += fmt.Sprintf("%s: Xv1=%.2f Xv2=%.2f Xv3=%.2f\n",
		style.Render("Rotation (°/s)"),
		m.rot.Xv1, m.rot.Xv2, m.rot.Xv3)
	s += fmt.Sprintf("%s: Xv1=%.2f Xv2=%.2f Xv3=%.2f\n",
		style.Render("Accel (m/s^2)"),
		m.acc.Xv1, m.acc.Xv2, m.acc.Xv3)
	s += fmt.Sprintf("%s: Tfog=%.1f Tacc=%.1f Tsens=%.1f\n",
		style.Render("Temperature (°C)"),
		m.temp.Fog, m.temp.Acc, m.temp.Sens)

	if (m.status.Word[0] & ixblue.AlgDvlValid) != 0 {
		s += fmt.Sprintf("%s: Long=%.2f Trans=%.2f Vert=%.2f\n",
			style.Render("DVL (m/s)"),
			m.dvl.Speed[0], m.dvl.Speed[1], m.dvl.Speed[2])
	} else {
		s += fmt.Sprintf("%s: %s=%.2f %s=%.2f %s=%.2f\n",
			style.Render("DVL (m/s)"),
			alert.Render("Long"), m.dvl.Speed[0],
			alert.Render("Trans"), m.dvl.Speed[1],
			alert.Render("Vert"), m.dvl.Speed[2])
	}

	return s
}

func (m model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.String() {
		case "ctrl+c", "q":
			close(m.done)
			return m, tea.Quit
		}
	case errMsg:
		m.err = msg
		return m, tea.Quit
	case ixblue.DataRecord:
		m.updateData(msg)
		return m, waitForData(m.src)
	}

	return m, nil
}

func (m model) Init() tea.Cmd {
	switch m.u.Scheme {
	case "tcp":
		conn, err := net.Dial("tcp", m.u.Host)
		if err != nil {
			return tea.Quit
		}
		dec := ixblue.NewDecoder(conn)
		return tea.Batch(listenForData(dec, m.src, m.done),
			waitForData(m.src))
	case "nats":
		if len(m.u.Path) < 2 {
			return func() tea.Msg {
				return errMsg(errors.New("Invalid URL"))
			}
		}
		conn, err := stan.Connect(clusterID, "rovins-mon",
			stan.NatsURL(fmt.Sprintf("nats://%s", m.u.Host)))
		if err != nil {
			return func() tea.Msg {
				return errMsg(err)
			}
		}
		return tea.Batch(subscribeToData(conn, m.u.Path[1:], m.src, m.done),
			waitForData(m.src))
	}

	return tea.Quit
}

func main() {
	args := parseCmdLine()
	if len(args) == 0 {
		flag.Usage()
		os.Exit(1)
	}

	u, err := url.Parse(args[0])
	if err != nil {
		fmt.Fprintf(os.Stderr, "Cannot parse url, %q: %v", args[0], err)
		os.Exit(1)
	}

	p := tea.NewProgram(model{
		u:    u,
		src:  make(chan ixblue.DataRecord, 1),
		done: make(chan struct{}, 1),
	})

	p.EnterAltScreen()
	defer p.ExitAltScreen()
	if p.Start() != nil {
		fmt.Fprintln(os.Stderr, "Cannot start program")
		return
	}
}
