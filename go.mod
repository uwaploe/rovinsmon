module bitbucket.org/uwaploe/rovinsmon

go 1.15

require (
	bitbucket.org/uwaploe/ixblue v0.3.2
	github.com/charmbracelet/bubbletea v0.13.1
	github.com/charmbracelet/lipgloss v0.1.2
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/nats-io/nats-server/v2 v2.2.1 // indirect
	github.com/nats-io/nats-streaming-server v0.21.1 // indirect
	github.com/nats-io/stan.go v0.8.3
)
